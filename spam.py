try:
    import pyautogui
    import keyboard
except:
    print("ERROR! Can't import the requirements, please install it! \n\npython3 -m pip install -r requirements.txt")
    quit()
from time import sleep
import string
import random

print("Welcome to automatic spamer for messaging apps\n\n")

prot = input("First of all... Does your app have anti fast sending messages protection? ")
if prot[0].lower() == "y":
    delay = float(input("Ok, please enter an delay time: "))
else:
    print("Ok, skipping")
    delay = None

print("\nPlease select the position of typebox")
print("Press enter when you are fine with position\n")

while True:
    x, y = pyautogui.position()
    positionStr = 'X: ' + str(x).rjust(4) + ' Y: ' + str(y).rjust(4)
    print(positionStr, end='')
    print('\b' * len(positionStr), end='', flush=True)

    if keyboard.is_pressed("enter"):
    	tmp = input()
    	break
    else:
    	pass

print("""
    What type of message do you want to send?
    1. Random
    2. Fixed
    """)
msg_type = int(input("Enter your choice: "))

if msg_type == 1:
    msg = None
    fl = input("\nOK, do you wanna use a fixed lenght of message? ")
    if fl[0].lower() == "y":
        msg_len = int(input("\nType a number of characters: "))
    else:
        msg_len = None
        max_len = int(input("\nOK, what's the max lenght for message? "))

elif msg_type == 2:
    msg = input("\nOK, enter your message: ")
else:
    print("Invalid selection, exiting...")
    quit()

counter = input("\nDo you want to add a counter at the start of messages? ")
if counter[0].lower() == "y":
    counter = True
else:
    counter = False

nr = int(input("\nNumber of messages: "))

print("\n\nAllright! Starting right now, enjoy!\n")
pyautogui.click(x, y)

for i in range(nr):
    if delay != None:
        sleep(delay)
    print("Sending message {}".format(i+1))

    if msg == None:
        random_chars = string.ascii_letters + string.digits + string.punctuation
        if msg_len == None:
            if counter == True:
                pyautogui.typewrite(str(i+1)+'. ' + ''.join(random.choice(random_chars) for i in range(random.randint(1, max_len))))
            else:
                pyautogui.typewrite(''.join(random.choice(random_chars) for i in range(random.randint(1, max_len))))
        else:
            if counter == True:
                pyautogui.typewrite(str(i+1)+'. ' + ''.join(random.choice(random_chars) for i in range(msg_len)))
            else:
                pyautogui.typewrite(''.join(random.choice(random_chars) for i in range(msg_len)))
    
    else:
        if counter == True:
            pyautogui.typewrite(str(i+1)+'. ' + (msg))
        else:
            pyautogui.typewrite(msg)

    pyautogui.press("enter")

print("\nAll done")
